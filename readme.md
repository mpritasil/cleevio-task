#Cleevio test task
Kód lze spustit, ale rozumné odpovědi nedává, protože datové zdroje nejsou naimplementovány. Je alespoň vidět error response.

##Výstup

###Error
```json
{ 
  "error":
    {
      "code":"400",
      "message":"Id must be an positive whole number, \"-2\" passed.",
    }
}
```
### Success
```json
{
"data": 
   {
        "identification": 1,  
        "title": "Watch with water fountain",
        "price": 200,
        "description": "Beautifully crafted timepiece for every gentleman. "
    }
}
```

##Controller
Controller nevrací json ani nenastavuje http header. Myšlenka je taková, že o toto by se měla starat jiná komponenta.
Controller vrací pole, které po zakodování pomocí json_encode bude vypadat jako v případech výše. Velmi naivní použítí
výstupu controlleru budiž v **www/index.php**

##Model
Modelu je předáno obecné db rozhraní a obecné cache rozhraní, se kterými pracuje. Samotnou implementaci db nebo cahce nezná.
Přepínat mezi jednotlivými implementacemi lze pomocí souboru **config/cong.neon** (použil jsem nette/di).

##Datové zdroje
MySqlWatchDatabase a XmlWatchDatabase rozšiřují obecnou třídu WatchDatabase a implmentují jednotlivá rozhraní.
Takto lze dle mého nejlépe zaručit, že model neví jaká databáze je skutečně použita a současně nebude problém kdykoliv přidat
datový zdroj další. Přepínat mezi jednotlivými db lze jednoduše v **config.neon**. WatchDatabse vždy vrací WatchDTO objekt.
<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Models;

use Api\Classes\Cache;
use Api\Classes\Database\WatchDatabase;


class WatchModel
{

	private $cache;

	private $context;

	private $db;

	public function __construct(Cache $cache, WatchDatabase $db)
	{
		$this->cache = $cache;
		$this->context = get_class($this); // this will be more elaborate in real life, ideally some method in master model (getName())...
		$this->db = $db;
	}

	/**
	 * Gets watch by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function getById(int $id){
		$storageId = $this->getStorageId($id);

		// look in cache
		if($this->cache->has($storageId)){
			return $this->cache->get($storageId);
		}

		// retrieve from db
		$item = $this->db->get($id);

		// set cache if success
		if(isset($item)){
			$this->cache->set($storageId, $item);
		}

		return $item;
	}

	/**
	 * Returns id for cache storage
	 *
	 * @param int $id item id
	 *
	 * @return string
	 */
	private function getStorageId(int $id): string{
		return md5($this->context.'.'.$id);
	}

	/**
	 * Validates id
	 *
	 * @param $id
	 */
	public function validateId($id){
		if(!ctype_digit($id) || (int) $id < 1)
		{
			throw new \InvalidArgumentException(sprintf('Id must be an positive whole number, "%s" passed.', $id));
		}
	}

}
<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Classes;

class Response
{
	public static function getErrorResponse($code, string $message): array
	{
		// todo check code if its valid http status code
		return [
			'error' => [
				'code'    => (string) $code,
				'message' => $message
			]
		];
	}

	public static function getResponse($data): array {
		return [
			'data'=>$data
		];
	}
}
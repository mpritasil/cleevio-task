<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Classes;

class SimpleCache implements Cache {

	/**
	 * Stores data in cache
	 *
	 * @param string $id
	 * @param mixed  $data
	 *
	 * @return mixed
	 */
	public function set(string $id, $data)
	{
		// TODO: Implement set() method.
	}

	/**
	 * Retrieves data from cache
	 *
	 * @param string $id
	 *
	 * @return mixed
	 */
	public function get(string $id)
	{
		// TODO: Implement get() method.
		return null;
	}

	/**
	 * Checks if key exists
	 *
	 * @param string $id
	 *
	 * @return bool
	 */
	public function has(string $id): bool
	{
		// TODO: Implement has() method.
		return false;
	}
}
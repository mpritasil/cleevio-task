<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Classes;

abstract class HttpCodes{
	const SUCCESS = 200;
	const BAD_REQUEST = 400;
	const NOT_FOUND = 404;
	const SERVER_ERROR = 500;
}
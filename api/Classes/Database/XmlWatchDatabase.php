<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Classes\Database;

use Api\Classes\WatchDTO;
use Api\Exceptions\Xml\XmlLoaderException;

class XmlWatchDatabase extends WatchDatabase implements XmlWatchLoader
{

	/**
	 * @param int $id
	 *
	 * @return WatchDTO|null
	 * @throws \Exception
	 */
	public function get(int $id)
	{
		try
		{
			$item = $this->loadByIdFromXml((string) $id);

			if (isset($item))
			{
				$item = new WatchDTO(...$item); // transform to DTO
			}

			return $item;
		}
		catch (XmlLoaderException $xmlE)
		{ // mask specific exception as some general server exception
			// log XmlException
			throw new \Exception('Internal server error.', 500, $xmlE);
		}
	}

	/**
	 * @param string $watchIdentification
	 *
	 * @return array|null
	 *
	 * @throws XmlLoaderException May be thrown on a fatal error, such as
	 * XML file containing data of watches
	 * could not be loaded or parsed.
	 */
	public function loadByIdFromXml(string $watchIdentification): array
	{
		// TODO: Implement loadByIdFromXml() method.
	}
}
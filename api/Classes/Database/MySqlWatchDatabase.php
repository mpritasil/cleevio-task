<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Classes\Database;

use Api\Classes\WatchDTO;
use Api\Exception\MySql\MySqlWatchNotFoundException;
use Api\Exceptions\MySql\MySqlRepositoryException;

class MySqlWatchDatabase extends WatchDatabase implements MySqlWatchRepository
{

	/**
	 * @param int $id
	 *
	 * @return WatchDTO|null
	 * @throws \Exception
	 */
	public function get(int $id)
	{
		try
		{
			$item = $this->getWatchById($id);

			return $item;
		}
		catch (MySqlWatchNotFoundException $notFound)
		{
			// log $notFound
			return null;
		}
		catch (MySqlRepositoryException $repoE) // mask this exception with some general exception
		{
			// log $repoE
			throw new \Exception('Internal server error', 500, $repoE);
		}
	}

	/**
	 * @param int $id
	 *
	 * @return WatchDTO
	 *
	 * @throws MySqlWatchNotFoundException Is thrown when the watch could
	 * not be found in a mysql
	 * Database, eg. watch with the
	 * associated id does not exist.
	 *
	 * @throws MySqlRepositoryException May be thrown on a fatal error,
	 * such as connection
	 * to a Database failed.
	 */
	public function getWatchById(int $id): WatchDTO
	{
		// TODO: Implement getWatchById() method.
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Classes\Database;

use Api\Classes\WatchDTO;

abstract class WatchDatabase{

	/**
	 * Gets watch by id
	 *
	 * @param int $id
	 *
	 * @return WatchDTO|null
	 *
	 * @throws \Exception on error
	 *
	 */
	public abstract function get(int $id);

}

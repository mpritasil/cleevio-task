<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

use Api\Exceptions\Xml\XmlLoaderException;

declare(strict_types=1);

namespace Api\Classes\Database;

interface XmlWatchLoader
{
	/**
	 * @param string $watchIdentification
	 *
	 * @return array|null
	 *
	 * @throws XmlLoaderException May be thrown on a fatal error, such as
	 * XML file containing data of watches
	 * could not be loaded or parsed.
	 */
	public function loadByIdFromXml(string $watchIdentification) : array;
}
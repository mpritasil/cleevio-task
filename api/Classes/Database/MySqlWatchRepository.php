<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Classes\Database;

use Api\Classes\WatchDTO;
use Api\Exception\MySql\MySqlWatchNotFoundException;
use Api\Exceptions\MySql\MySqlRepositoryException;

interface MySqlWatchRepository {

	/**
	 * @param int $id
	 *
	 * @return WatchDTO
	 *
	 * @throws MySqlWatchNotFoundException Is thrown when the watch could
	 * not be found in a mysql
	 * Database, eg. watch with the
	 * associated id does not exist.
	 *
	 * @throws MySqlRepositoryException May be thrown on a fatal error,
	 * such as connection
	 * to a Database failed.
	 */
	public function getWatchById(int $id): WatchDTO;
}
<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Classes;

interface Cache {

	/**
	 * Stores data in cache
	 *
	 * @param string $id
	 * @param mixed $data
	 *
	 * @return mixed
	 */
	public function set(string $id, $data);

	/**
	 * Retrieves data from cache
	 *
	 * @param string $id
	 *
	 * @return mixed
	 */
	public function get(string $id);

	/**
	 * Checks if key exists
	 *
	 * @param string $id
	 *
	 * @return bool
	 */
	public function has(string $id): bool;
}


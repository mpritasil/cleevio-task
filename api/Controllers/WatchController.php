<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Controllers;

use Api\Classes\HttpCodes;
use Api\Classes\Response;
use Api\Classes\WatchDTO;
use Api\Models\WatchModel;

class WatchController
{

	private $model;

	public function __construct(WatchModel $model)
	{
		$this->model = $model;
	}

	/**
	 * Tries to found watch by id
	 *
	 * @param $id
	 *
	 * @return array
	 */
	public function getByIdAction($id)
	{
		try
		{
			$this->model->validateId($id);
			$item = $this->model->getById((int)$id);

			if (!$item instanceof WatchDTO){
				return Response::getErrorResponse(HttpCodes::NOT_FOUND, sprintf('Watch with id %d not found. ', $id));
			}

			return Response::getResponse($item);
		}
		catch (\InvalidArgumentException $invArgE)
		{
			//log
			return Response::getErrorResponse(HttpCodes::BAD_REQUEST, $invArgE->getMessage());
		}
		catch (\Throwable $e)
		{
			//log
			return Response::getErrorResponse(HttpCodes::SERVER_ERROR, $e->getMessage());
		}
	}


}

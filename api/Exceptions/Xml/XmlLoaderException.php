<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Exceptions\Xml;

use RuntimeException;

class XmlLoaderException extends RuntimeException { };
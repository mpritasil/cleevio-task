<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */


declare(strict_types=1);

namespace Api\Exception\MySql;

use Api\Exceptions\MySql\MySqlRepositoryException;


class MySqlWatchNotFoundException extends MySqlRepositoryException {

	public function __construct(string $message = "", $code, Throwable $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}
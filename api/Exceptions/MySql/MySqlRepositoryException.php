<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Exceptions\MySql;

use RuntimeException;

class MySqlRepositoryException extends RuntimeException {}
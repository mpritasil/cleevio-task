<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Tests;

require_once 'bootstrap.php';

use Api\Models\WatchModel;
use Mockery;
use Tester\Assert;
use Tester\TestCase;

final class WatchModelTest extends TestCase
{

	public function tearDown()
	{
		Mockery::close();
	}


	/**
	 * @dataProvider dataProviderForTestValidate
	 */
	public function testValidate(string $id)
	{
		$cacheMock = Mockery::mock('Api\\Classes\\Cache');
		$dbMock = Mockery::mock('Api\\Classes\\Database\\WatchDatabase');

		$model = new WatchModel($cacheMock, $dbMock);

		Assert::exception(
			function () use ($model, $id){$model->validateId($id);},
			\InvalidArgumentException::class
		);
	}


	public function dataProviderForTestValidate(): array
	{
		return [
			[
				'11.1'
			],
			[
				'2,3'
			],
			[
				'ehm'
			],
			[
				'1e9'
			],
			[
				'-2'
			]
		];
	}


	public function testCacheSuccess()
	{
		$watchMock = $this->mockWatch();
		$cacheMock = $this->mockCacheSuccess($watchMock);
		$dbMock = Mockery::mock('Api\\Classes\\Database\\WatchDatabase');

		$model = new WatchModel($cacheMock, $dbMock);

		Assert::same($watchMock, $model->getById(1));
	}

	public function testCacheFailDbFail(){
		$cacheMock = $this->mockCacheFail();
		$dbMock = Mockery::mock('Api\\Classes\\Database\\WatchDatabase')->shouldReceive(['get' => null])->getMock();

		$model = new WatchModel($cacheMock, $dbMock);

		Assert::equal(null, $model->getById(1));
	}

	public function testCacheFailDbSuccess(){
		$watchMock = $this->mockWatch();
		$cacheMock = $this->mockCacheFail();
		$cacheMock->shouldReceive('set');
		$dbMock = $this->mockDbSuccess($watchMock);

		$model = new WatchModel($cacheMock, $dbMock);

		Assert::same($watchMock, $model->getById(1));
	}

	public function mockCacheSuccess($item){
		return Mockery::mock('Api\\Classes\\Cache')->shouldReceive(['has' => true, 'get' => $item])->getMock();
	}


	public function mockDbSuccess($item){
		return Mockery::mock('Api\\Classes\\Database\\WatchDatabase')->shouldReceive(['get' => $item])->getMock();
	}

	public function mockCacheFail(){
		return Mockery::mock('Api\\Classes\\Cache')->shouldReceive(['has' => false])->getMock();
	}


	public function mockWatch(){
		return Mockery::mock('Api\Classes\\WatchDTO');
	}

}

(new WatchModelTest())->run();
<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */

declare(strict_types=1);

namespace Api\Tests;

require_once 'bootstrap.php';


use Api\Classes\HttpCodes;
use Api\Classes\WatchDTO;
use Api\Controllers\WatchController;
use Api\Models\WatchModel;
use Mockery;
use Tester\Assert;
use Tester\TestCase;


final class WatchControllerTest extends TestCase
{
	public function tearDown()
	{
		Mockery::close();
	}

	/**
	 * @dataProvider dataProviderForTestGetByIdInvalidId
	 */
	public function testGetByIdInvalidId(string $expected)
	{
		$modelMock = Mockery::mock('Api\\Models\\WatchModel')->shouldReceive('validateId')->andThrow(\InvalidArgumentException::class)->getMock();

		$controller = new WatchController($modelMock);


		Assert::equal(json_decode($expected, true), $controller->getByIdAction(1));
	}

	public function dataProviderForTestGetByIdInvalidId(): array
	{
		return [
			[
				'{"error": {"code": "' . HttpCodes::BAD_REQUEST . '", "message": ""}}'
			]
		];
	}

	public function testGetByIdNotFound()
	{
		$modelMock = Mockery::mock('Api\\Models\\WatchModel')->shouldReceive(['validateId' => true, 'getById' => null])->getMock();
		$controller = new WatchController($modelMock);
		Assert::true(array_key_exists('error',$controller->getByIdAction(1)));
	}

	public function testGetByIdServerError()
	{
		$modelMock = Mockery::mock('Api\\Models\\WatchModel')->shouldReceive(['validateId' => true, 'getById' => null])->andThrow(\Exception::class)->getMock();
		$controller = new WatchController($modelMock);
		Assert::true(array_key_exists('error',$controller->getByIdAction(1)));
	}

	public function testGetById()
	{
		$watchMock = Mockery::mock('Api\\Classes\\WatchDTO');
		$modelMock = Mockery::mock('Api\\Models\\WatchModel')->shouldReceive(['validateId' => true, 'getById' => $watchMock])->getMock();
		$controller = new WatchController($modelMock);

		$response = $controller->getByIdAction(1);

		Assert::true(array_key_exists('data',$response));
		Assert::false(array_key_exists('error', $response));
	}

}

(new WatchControllerTest())->run();
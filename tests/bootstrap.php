<?php

declare(strict_types=1);

namespace Api\Tests;

use Tester\Environment;

require __DIR__ . '/../vendor/autoload.php';

date_default_timezone_set('UTC');

const TEST_DIR = __DIR__;

Environment::setup();

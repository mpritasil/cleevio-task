<?php
/**
 * Created by PhpStorm.
 * User: Marek
 */
declare(strict_types=1);

use Api\Classes\Response;

require __DIR__ . '/../vendor/autoload.php';


function setHeader($code)
{
	header('Content-Type: application/json');
	http_response_code((int)$code);
}

// init di container
$loader    = new Nette\DI\ContainerLoader('temp', true);
$class     = $loader->load(function ($compiler) {
	$compiler->loadConfig('../config/config.neon');
});
$container = new $class;


// some routing woodoo would be here

// only /watch/{id} url will work
try
{
	list($entity, $id) = explode('/', ltrim($_SERVER['REQUEST_URI'], '/'));

	if ($entity !== 'watch')
	{
		setHeader(404);
		die(json_encode(Response::getErrorResponse(404, 'Nope'), JSON_PRETTY_PRINT));
	}

	$controller = $container->getService('WatchController');
	$response   = $controller->getByIdAction($id);

	if (array_key_exists('error', $response))
	{
		setHeader($response['error']['code']);
		die(json_encode($response));
	}

	die(json_encode($response));
}
catch (Throwable $err)
{
	setHeader(404);
	die(json_encode(Response::getErrorResponse(404, 'Nope'), JSON_PRETTY_PRINT));
}
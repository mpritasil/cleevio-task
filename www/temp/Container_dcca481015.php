<?php
class Container_dcca481015 extends Nette\DI\Container
{
	protected $meta = [
		'types' => [
			'Api\Classes\Database\WatchDatabase' => [1 => ['db']],
			'Api\Classes\Database\MySqlWatchRepository' => [1 => ['db']],
			'Api\Classes\Database\MySqlWatchDatabase' => [1 => ['db']],
			'Api\Classes\Cache' => [1 => ['cache']],
			'Api\Classes\SimpleCache' => [1 => ['cache']],
			'Api\Controllers\WatchController' => [1 => ['WatchController']],
			'Api\Models\WatchModel' => [1 => ['WatchModel']],
			'Nette\DI\Container' => [1 => ['container']],
		],
		'services' => [
			'WatchController' => 'Api\Controllers\WatchController',
			'WatchModel' => 'Api\Models\WatchModel',
			'cache' => 'Api\Classes\SimpleCache',
			'container' => 'Nette\DI\Container',
			'db' => 'Api\Classes\Database\MySqlWatchDatabase',
		],
		'aliases' => [],
	];


	public function __construct(array $params = [])
	{
		$this->parameters = $params;
		$this->parameters += [];
	}


	public function createService__WatchController(): Api\Controllers\WatchController
	{
		$service = new Api\Controllers\WatchController($this->getService('WatchModel'));
		return $service;
	}


	public function createService__WatchModel(): Api\Models\WatchModel
	{
		$service = new Api\Models\WatchModel($this->getService('cache'), $this->getService('db'));
		return $service;
	}


	public function createServiceCache(): Api\Classes\SimpleCache
	{
		$service = new Api\Classes\SimpleCache;
		return $service;
	}


	public function createServiceContainer(): Nette\DI\Container
	{
		return $this;
	}


	public function createServiceDb(): Api\Classes\Database\MySqlWatchDatabase
	{
		$service = new Api\Classes\Database\MySqlWatchDatabase;
		return $service;
	}


	public function initialize()
	{
	}
}
